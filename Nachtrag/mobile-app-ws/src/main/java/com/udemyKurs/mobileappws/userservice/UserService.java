package com.udemyKurs.mobileappws.userservice;

import com.udemyKurs.mobileappws.ui.model.request.UserDetailsRequestModel;
import com.udemyKurs.mobileappws.ui.model.response.UserRest;

public interface UserService {
    UserRest createUser(UserDetailsRequestModel userDetails);
}
